//
//  MainScreen.swift
//  TinderEffect
//
//  Created by Bastien HERREROS on 12/01/2021.
//

import SwiftUI

struct MainScreen: View {
    
    @EnvironmentObject var modelData: ModelData
    
    var body: some View {
        
        VStack()
        {
            
            HStack(){
                Button(action: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/{}/*@END_MENU_TOKEN@*/) {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Content@*/Text("Fav")/*@END_MENU_TOKEN@*/
                }
                Spacer()
                Button(action: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/{}/*@END_MENU_TOKEN@*/) {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Content@*/Text("Option1")/*@END_MENU_TOKEN@*/
                }
                Button(action: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/{}/*@END_MENU_TOKEN@*/) {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Content@*/Text("Option2")/*@END_MENU_TOKEN@*/
                }
                Spacer()
                Button(action: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/{}/*@END_MENU_TOKEN@*/) {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Content@*/Text("Profile")/*@END_MENU_TOKEN@*/
                }
                
            }
            
            Spacer()
            
            
            
            let charNullable = modelData.characters.squadmates.randomElement();
            
            if var char = charNullable
            {
                CharacterProfile(
                    title: char.name + ", " + char.gender + ", " + char.race + ", " /*+ char.classe*/,
                    quote: char.quote,
                    urlImage: char.img)
            
                Spacer()
                
                HStack
                {
                    ForEach(Choice.allCases){choice in
                        if(choice != Choice.empty)
                        {
                            Button(action:
                            {
                                char.choice = choice
                            }){
                                Text(choice.rawValue)
                            }
                            
                            
                        }
                    }
                    
                    
                    /*Choice.allCases.forEach({
                        if($0 != Choice.empty)
                        {
                            Button(action: {
                                char.choice = $0
                            }){}
                        }
                        
                    })*/
                    
                }
                
            }
            
        }.onAppear
        {
            modelData.download​()
        }

    }
    

}

struct MainScreen_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen().environmentObject(ModelData())
    }
}



struct ExtractedView: View {
    var body: some View {
        Text("Super hot")
    }
}
