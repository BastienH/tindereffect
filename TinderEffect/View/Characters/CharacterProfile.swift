//
//  CharacterProfile.swift
//  TinderEffect
//
//  Created by Bastien HERREROS on 12/01/2021.
//

import SwiftUI

struct CharacterProfile: View {
    
    public init(title: String, quote: String, urlImage: String) {
        self.title = title
        self.quote = quote
        self.urlImage = urlImage
    }
    
    @ObservedObject private var pp = ProfilePicture()
    
    private var title : String
    private var quote : String
    private var urlImage : String
    
    var body: some View {
        
        VStack()
        {
            if let image = pp.uiimage
            {
                Image(uiImage: image)
                    .resizable()
                    
                    .padding(/*@START_MENU_TOKEN@*/.horizontal, 20.0/*@END_MENU_TOKEN@*/)
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/2.0)
                
            }
            
            Text(title)
                .fontWeight(.bold)
                .padding(/*@START_MENU_TOKEN@*/.horizontal, 20.0/*@END_MENU_TOKEN@*/)
                .padding(.bottom, 5.0)
            
            
            Text(quote)
                .padding(/*@START_MENU_TOKEN@*/.horizontal, 20.0/*@END_MENU_TOKEN@*/)
                .multilineTextAlignment(.leading)
                .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
                .padding(.top, 5.0)
            
        }
        .onAppear
        {
            let url = URL(string: urlImage)
            pp.load(url: url!)
        }
    }
}

struct CharacterProfile_Previews: PreviewProvider {
    static var previews: some View {
        CharacterProfile(title :"Ashley Williams, Female, Human, Soldier",
                         quote:"Ashley Williams is a human soldier who served in the Systems Alliance as a Gunnery Chief in the 2nd Frontier Division on Eden Prime, and was later assigned to Commander Shepard's squad after the geth attack on Eden Prime.",
                         urlImage: "https://i.pinimg.com/originals/c6/5d/a2/c65da2052bea6c890aecea5db9d08199.jpg")
    }
}
