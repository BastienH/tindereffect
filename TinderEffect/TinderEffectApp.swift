//
//  TinderEffectApp.swift
//  TinderEffect
//
//  Created by Bastien HERREROS on 12/01/2021.
//

import SwiftUI

@main
struct TinderEffectApp: App {
    @StateObject private var modelData = ModelData()
    var body: some Scene {
        WindowGroup {
            MainScreen().environmentObject(modelData)
                
        }
    }
}
