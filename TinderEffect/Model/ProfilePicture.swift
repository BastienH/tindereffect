//
//  ProfilePicture.swift
//  TinderEffect
//
//  Created by Bastien HERREROS on 19/01/2021.
//

import Foundation
import SwiftUI

class ProfilePicture: ObservableObject
{
    @Published var uiimage: UIImage? = nil
    
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self!.uiimage = image
                    }
                }
            }
        }
    }
}

