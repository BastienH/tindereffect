//
//  Character.swift
//  TinderEffect
//
//  Created by Bastien HERREROS on 19/01/2021.
//

import Foundation
import SwiftUI
import CoreLocation

enum Choice: String, Hashable, Codable, CaseIterable, Identifiable{
    var id: String {return rawValue}
    
    case trash
    case meh
    case ok
    case cool
    case hot
    case empty
    
    
}

struct Character: Hashable, Codable {
    
    public init(id: Int, name: String, gender: String, race: String, img: String, quote: String, description: String, classe: String, isRomanceable: Bool) {
        self.characterId = id
        self.name = name
        self.gender = gender
        self.race = race
        self.img = img
        self.quote = quote
        self.description = description
        //self.classe = classe
        self.isRomanceable = isRomanceable
        self.choice = Choice.empty
    }
    
    var characterId: Int
    var name: String
    var gender: String
    var race: String
    var img: String
    var quote: String
    var description: String
    //var classe: String
    var isRomanceable : Bool
    var choice : Choice
    
    private enum CodingKeys: String, CodingKey {
            case characterId
            case name
            case gender
            case race
            case img
            case quote
            case description
            case isRomanceable
        }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        characterId = try container.decode(Int.self, forKey: .characterId)
        name = try container.decode(String.self, forKey: .name)
        gender = try container.decode(String.self, forKey: .gender)
        race = try container.decode(String.self, forKey: .race)
        img = try container.decode(String.self, forKey: .img)
        quote = try container.decode(String.self, forKey: .quote)
        description = try container.decode(String.self, forKey: .description)
        isRomanceable = try container.decode(Bool.self, forKey: .isRomanceable)
        
        choice = Choice.empty
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(characterId, forKey: .characterId)
        try container.encode(name, forKey: .name)
        try container.encode(gender, forKey: .gender)
        try container.encode(race, forKey: .race)
        try container.encode(img, forKey: .img)
        try container.encode(quote, forKey: .quote)
        try container.encode(description, forKey: .description)
        try container.encode(isRomanceable, forKey: .isRomanceable)
    }
    
}
