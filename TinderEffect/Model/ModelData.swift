//
//  ModelData.swift
//  TinderEffect
//
//  Created by Bastien HERREROS on 19/01/2021.
//

import Foundation
import Combine

struct Characters: Hashable, Codable
{
    public var squadmates : [Character]
    
    public init()
    {
        squadmates = []
    }
}


final class ModelData: ObservableObject {
    @Published var characters: Characters = Characters()
    
    func download​()
    {
        guard let url = URL(string: "https://mass-effect-api.herokuapp.com/characters") else {return}
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request){ (data, reponse, error) in
            if let error = error {print(error)}
            else if let data = data
            {
                do{
                    let jsonDecoder = JSONDecoder()
                    
                    let region = try jsonDecoder.decode(Characters.self, from: data)
                    
                    DispatchQueue.main.async {
                        self.characters = region
                    }
                    
                }catch let error
                {
                    print(error)
                }
            }
        }
        .resume()
        

    }
}

/*Character(id: 1, name: "name", gender: "gender", race: "race", img: "https://i.pinimg.com/originals/c6/5d/a2/c65da2052bea6c890aecea5db9d08199.jpg", quote: "quote", description: "description", classe: "classe", isRomanceable: true)*/




